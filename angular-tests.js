test( "hello test", function() {
    ok( 1 == "1", "Passed!" );
});



test('CartController Items Size', function() {
    var $controller = injector.get('$controller');
    $controller('CartController', {
        $scope: this.$scope
    });
    equal(this.$scope.getItemsSize(), 3);
});

test('TicketHeaderController test "totalTicket" function', function() {
    var $controller = injector.get('$controller');
    $controller('TicketHeaderController', {
        $scope: this.$scope
    });
    
    equal(this.$scope.ticketHeaders.length, 3);
    var ticketHeaderInstance = this.$scope.ticketHeaders[0];
    equal(ticketHeaderInstance.ticketDetails.length, 3);
    equal(this.$scope.totalTicket(ticketHeaderInstance), 36);
});

/*
test('TicketHeaderController test "employeeTotals" function', function() {
    var $controller = injector.get('$controller');
    $controller('TicketHeaderController', {
        $scope: this.$scope
    });
    console.log();
    equal(this.$scope.employeeTotals(), 3);
});
*/

