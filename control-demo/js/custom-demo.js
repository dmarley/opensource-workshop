//angular.module('ControlDemo', ['ui.bootstrap']);
var app = angular.module('myApp', ['ui.bootstrap','ngGrid']);
function TicketController($scope) {
    // tab values
    $scope.panes = [
        { title:"Employees", content:"employee" },
        { title:"Blank", content:"blank" }
    ];
    // pagination values
    $scope.noOfPages = 7;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    // pagination methods
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    // accordion stuff
    $scope.oneAtATime = false;

    // ticket details
    $scope.ticketHeaders = [
        {type: 'labour', ticketNumber: '00', ticketDate: '10-03-13', totalAmount: 5944, customer: '0001',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 2, qty: 42, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 3, qty: 12, jdeNumber:'23444'}]
        },
        {type: 'labour', ticketNumber: '01', ticketDate: '10-03-13', totalAmount: 5944, customer: '0002',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 4, qty: 12, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 5, qty: 12, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 6, qty: 13, jdeNumber:'23444'}]},
        {type: 'labour', ticketNumber: '02', ticketDate: '10-03-13', totalAmount: 5944, customer: '0003',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 7, qty: 12, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 8, qty: 02, jdeNumber:'23444'},
                {dateWorked: '2012-06-14', lineNumber: 9, qty: 14, jdeNumber:'23444'}]}
    ];

    $scope.ticketDetailsGrid = $scope.ticketHeaders;

    $scope.jobs = {};
    $scope.jobsarray = [];
    $scope.jobOptions = {};
    $scope.jobDetailsOptionsSet = [];
    // json
    $.getJSON("data/jobs.json",function(data){
        $scope.jobs=data;
/*
        $.each($scope.jobs.jobs.registrations, function(k, v) {
            console.log("key: " + k + ", value: " + v);
            $scope.jobsarray.push(v);
        });
*/
        angular.forEach($scope.jobs.jobs.registrations, function(registration) {
            $scope.jobsarray.push(registration);
//            console.log(registration)
        });
//        console.log($scope.jobs.jobs);
//        console.log($scope.jobsarray);
    });
    $scope.jobOptions = { data: 'jobs.jobs' };
    $scope.jobDetailsOptionsSet =
    {
        data: 'jobsarray',
        enableColumnReordering: true,
        enableColumnResize: true
    };


    // ng-grid
    $scope.gridOptions = {
        data: 'ticketHeaders',
        columnDefs: [
            {field: 'type'},
            {field: 'ticketDate'},
            {field: 'totalAmount'},
            {field: 'customer'}
        ]
    }

    $scope.gridDetailOptionsSet = [];

    for (var i = 0; i < $scope.ticketHeaders.length; i++) {
        $scope.gridDetailOptionsSet.push(
            {
                data: "ticketHeaders[" + parseInt(i) + "].ticketDetails",
                enableColumnResize: true,
                enableColumnReordering: true,
                enableSorting: true,
                maintainColumnRatios: true,
                showFilter: true
            }
        );
    }

/*
    $scope.setGridData = function(obj) {
        $scope.gridOptions.data = obj;
    }
*/

/*
    $scope.currentTicketHeaderIndex = -1;
    $scope.incrementGridData = function() {
        $scope.currentTicketHeaderIndex++;
        $scope.ticketDetailsGrid = $scope.ticketHeaders[$scope.currentTicketHeaderIndex].ticketDetails;
    }
*/
};
