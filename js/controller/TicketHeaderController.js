
function TicketHeaderController($scope) {
    $scope.jdeNumbers = [];
    $scope.hours = [];
    $scope.ticketHeaders = [
        {type: 'labour', ticketNumber: '00', ticketDate: '10-03-13', totalAmount: 5944, customer: '0001',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'}]
        },
        {type: 'labour', ticketNumber: '01', ticketDate: '10-03-13', totalAmount: 5944, customer: '0001',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'}]},
        {type: 'labour', ticketNumber: '02', ticketDate: '10-03-13', totalAmount: 5944, customer: '0001',
            ticketDetails: [{dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
                            {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'}]}
    ];

    $scope.remove = function(ticketHeader, index) {
        ticketHeader.ticketDetails.splice(index,1)
    }

    $scope.add = function(ticketHeader) {
        ticketHeader.ticketDetails.splice(ticketHeader.ticketDetails.length,0, {dateWorked:'2013-04-13',lineNumber:0,qty:0,jdeNumber:'XXXXXX'});
    }

    $scope.changeQty = function(ticketHeader) {
        totalTicket(ticketHeader);

    };

    $scope.totalTicket = function(ticketHeader) {
        $scope.employeeTotals();
        var total = 0;
        angular.forEach(ticketHeader.ticketDetails, function(ticketDetail) {
            total = total + parseInt(ticketDetail.qty)
        });
        return total;
    }


    $scope.selectTicketDetail = function(row) { $scope.selectedRow = row; };

    $scope.employeeTotals = function() {
        var locEmp = [];
        var locHours = [];

        angular.forEach($scope.ticketHeaders, function(ticketHeader) {
           angular.forEach(ticketHeader.ticketDetails, function(ticketDetail){
               var empIndex = 0;
               for (empIndex=0; empIndex < locEmp.length; empIndex++) {
                   if (ticketDetail.jdeNumber == locEmp[empIndex]) {
                       break;
                   }
               }
               // console.log(empIndex);
               if (empIndex < 0 || empIndex >= locEmp.length) {
                  locEmp.push(ticketDetail.jdeNumber);
                   locHours.push(parseInt(ticketDetail.qty));
               } else {
                   locHours[empIndex] = locHours[empIndex] + parseInt(ticketDetail.qty);
               }

           });
        });
        $scope.jdeNumbers=locEmp;
        $scope.hours=locHours;
    }
}