function TicketDetailController($scope) {
    $scope.ticketDetail = [
        {dateWorked: '2012-06-14', lineNumber: 1, qty: 12, jdeNumber:'23444'},
        {dateWorked: '2012-06-14', lineNumber: 2, qty: 8,  jdeNumber:'111111'},
        {dateWorked: '2012-06-14', lineNumber: 3, qty: 12, jdeNumber:'222222'}
    ];

    $scope.totalLines = function() {
        return $scope.ticketDetail.length;
    }

    function totalLines(newValue, oldValue, scope) {
        var total = 0;
        angular.forEach($scope.ticketDetail, function(ticketDetail) {
            total += ticketDetail.qty
        });
        $scope.ticketHeader.totalAmount = total;
    }

    $scope.$watch($scope.ticketDetail, $scope.totalLines());
}
