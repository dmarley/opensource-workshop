<!DOCTYPE html>
<html ng-app>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css" media="screen">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.min.css')}" type="text/css" media="screen">
    <g:javascript src="/controller/JobController.js"/>
    <script src="http://code.jquery.com/jquery.js"></script>
    <g:javascript src="angular.min.js"/>
    <g:javascript src="bootstrap.min.js"/>
</head>
<body ng-controller='JobController'>
    <div class="container-fluid">
        <div class="row-fluid">
            <table class="table">
                <tr ng-repeat="instance in jobs.job.jobInstances">
                    <td>{{instance.resource}}</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
