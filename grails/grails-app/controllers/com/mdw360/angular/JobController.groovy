package com.mdw360.angular

import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.converters.JSON

class JobController {

    def job() {
        def url =  "http://caswpsw002:7034/fief-spring-batch/jobs/job1.json";
        RestBuilder rest=new RestBuilder()
        render rest.get(url).getBody()
    }
}
